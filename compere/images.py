# -*- coding: utf-8 -*-

# Licensed under the MIT license
# http://opensource.org/licenses/mit-license.php

# Copyright 2007, Frank Scholz <coherence@beebits.net>

import os
import random

# Twisted
from twisted.internet import reactor
from twisted.internet import task
from twisted.web import client

# Pygame
import pygame
from pygame.locals import *

# Coherence
from coherence.base import Coherence
from coherence.upnp.core import DIDLLite
import louie

from menus import Menu


class Images(object):

    def __init__(self, controller, main_rect):
        self.controller = controller
        self.screen = pygame.Surface((main_rect.width, main_rect.height))
        self.images = []
        self.container_watch = []
        self.image_index = -1
        self.displaying = None
        self.current_image = None
        self.current_title = ''
        self.client = {}
        self.local_store = None

        buttons = (('save', (44, 50), None, None),
                   ('backward', (31, 140), self.proceed_to_previous_image, None),
                   ('pause', (31, 216), self.pause, 'play.png'),
                   ('forward', (31, 300), self.proceed_to_next_image, None),
                   ('mark-junk', (44, 380), self.mark_junk, None))
        self.right_menu = Menu(controller, 'right', buttons)

        self.display_loop = task.LoopingCall(self.proceed_to_next_image, skip_junk=True)
        louie.connect(self.media_server_found, 'Coherence.UPnP.ControlPoint.MediaServer.detected', louie.Any)
        louie.connect(self.media_server_removed, 'Coherence.UPnP.ControlPoint.MediaServer.removed', louie.Any)
        #self.log_loop = task.LoopingCall(self.print_state)
        #self.log_loop.start(10)

    def print_state(self):
        print "state:", len(self.images), self.image_index, self.display_loop.running

    def check(self, button, pos):
        pass

    def toggle_fullscreen(self, size, fullscreen, bestdepth):
        print "images toggle_fullscreen", size
        pygame.display.update(self.displaying)
        self.screen = pygame.display.set_mode(size, fullscreen, bestdepth)
        self.displaying = self.current_image.get_rect(center=((self.screen.get_width() / 2) - 1, (self.screen.get_height() / 2) - 1))
        self.screen.blit(self.current_image, self.displaying, (0, 0, self.displaying.width, self.displaying.height))
        pygame.display.update(self.displaying)

    def media_server_found(self, client, usn):
        print "images - media_server_found", client.device.get_friendly_name()
        if 'Flickr Images' == client.device.get_friendly_name():
            self.client[usn] = client
            #d = client.content_directory.browse(0, browse_flag='BrowseMetadata')
            d = self.client[usn].content_directory.browse(0, browse_flag='BrowseDirectChildren',
                                                        backward_compatibility=False)
            d.addCallback(self.process_media_server_browse, client)
            client.content_directory.subscribe_for_variable('ContainerUpdateIDs', self.state_variable_change)
            client.content_directory.subscribe_for_variable('SystemUpdateID', self.state_variable_change)

        if 'Coherence Test Content' == client.device.get_friendly_name():
            print "found a local MediaServer for storage"
            self.local_store = client
            d = client.content_directory.browse(0, browse_flag='BrowseDirectChildren',
                                                        backward_compatibility=False)
            d.addCallback(self.process_local_media_server_browse, client)

    def media_server_removed(self, usn):
        print "media_server_removed", usn
        if(isinstance(self.local_store, tuple) is True and
            len(self.local_store) == 2 and
            usn == self.local_store[0].device.get_usn()):
            self.local_store = None
            self.right_menu.set_button_action('save', None)
        if self.client.has_key(usn) == True:
            if self.display_loop.running:
                self.display_loop.stop()
            del self.client[usn]
            self.images = []

    def process_media_server_browse(self, results, client):
        print "process_media_server_browse", client
        for k, v in results.iteritems():
            #print k, v
            if k == 'items':
                for id, values in v.iteritems():
                    if values['upnp_class'].startswith('object.container'):
                        d = client.content_directory.browse(id, browse_flag='BrowseDirectChildren',
                                                                 backward_compatibility=False)
                        d.addCallback(self.process_media_server_browse, client)
                    if values['upnp_class'].startswith('object.item.imageItem'):
                        #print values
                        if values.has_key('resources'):
                            if(values.has_key('parent_id') and
                               values['parent_id'] not in self.container_watch):
                                self.container_watch.append(values['parent_id'])
                            if len(values['resources']):
                                title = values.get('title', 'untitled').encode('ascii', 'ignore')
                                self.images.append({'url': values['resources'].keys()[0], 'title': title})
                random.shuffle(self.images)
                if len(self.images):
                    if not self.display_loop.running:
                        self.display_loop.start(20, now=True)

    def process_local_media_server_browse(self, results, client):
        for k, v in results.iteritems():
            #print k, v
            if k == 'items':
                for id, values in v.iteritems():
                    #print values
                    if(values['upnp_class'].startswith('object.container') and
                       values['title'] == 'content'):
                        d = client.content_directory.browse(id, browse_flag='BrowseDirectChildren',
                                                                 backward_compatibility=False)
                        d.addCallback(self.process_local_media_server_browse, client)
                    if(values['upnp_class'].startswith('object.container') and
                       values['title'] == 'images'):
                        print "found object id", values['id'], "as storage point"
                        if hasattr(self, 'right_menu'):
                            self.right_menu.set_button_action('save', self.store_locally)
                        self.local_store = (self.local_store, values['id'])

    def state_variable_change(self, variable, usn):
        print variable.name, 'changed from', variable.old_value, 'to', variable.value
        if variable.old_value == '':
            return
        if variable.name == 'SystemUpdateID':
            if self.client.has_key(usn) == True:
                if len(self.images) == 0:
                    d = self.client[usn].content_directory.browse(0, browse_flag='BrowseDirectChildren',
                                                             backward_compatibility=False)
                    d.addCallback(self.process_media_server_browse, self.client[usn])

        if variable.name == 'ContainerUpdateIDs':
            changes = variable.value.split(',')
            while len(changes) > 1:
                container = changes.pop(0).strip()
                update_id = changes.pop(0).strip()
                if container in self.container_watch:
                    print "heureka, we have a change in ", container, ", container needs a reload"
                    if self.display_loop.running:
                        self.display_loop.stop()
                    self.images = []
                    d = self.client[usn].content_directory.browse(container, browse_flag='BrowseDirectChildren',
                                                             backward_compatibility=False)
                    d.addCallback(self.process_media_server_browse, self.client[usn])

    def loop_start(self, x=None, now=False):
        print "try loop_start", x, now, len(self.images), self.display_loop.running
        if len(self.images) and not self.display_loop.running:
            print "display_loop_start"
            self.display_loop.start(20, now)

    def loop_stop(self):
        print "loop_stop", self.display_loop.running
        if self.display_loop.running:
            self.display_loop.stop()

    def pause(self):
        if self.display_loop.running:
            self.display_loop.stop()
        elif len(self.images) and not self.display_loop.running:
            self.display_loop.start(20, now=True)

    def store_locally(self):
        print "store current image", self.image_index, self.images[self.image_index].get('url')
        if(isinstance(self.local_store, tuple) is True and
           len(self.local_store) == 2):
            print "at", self.local_store[0].device.get_friendly_name(), "in container id", self.local_store[1]
            d = self.local_store[0].content_directory.create_object(self.local_store[1],
                                                                {'title': self.images[self.image_index].get('title'),
                                                                 'upnp_class': 'object.item.imageItem.photo',
                                                                 'parentID': self.local_store[1]})

            def got_import_result(r, s, d):
                print "import from", s, "to", d, "was successful"

            def got_result(r, source_uri):
                print "store_locally got_result", r
                elt = DIDLLite.DIDLElement.fromString(r.get('Result', ''))
                for item in elt:
                    for res in item.findall('res'):
                        destination_uri = res.get('importUri', None)
                        if destination_uri is not None:
                            d = self.local_store[0].content_directory.import_resource(source_uri, destination_uri)
                            d.addCallback(got_import_result, self.images[self.image_index].get('url'), destination_uri)
                            d.addErrback(got_error)

            def got_error(error):
                print "store_locally got_error", error
            d.addCallback(got_result, self.images[self.image_index].get('url'))
            d.addErrback(got_error)

    def mark_junk(self):
        print "mark junk image", self.image_index, self.images[self.image_index].get('url')
        self.images[self.image_index]['junk'] = True
        self.proceed_to_next_image()

    def proceed_to_next_image(self, skip_junk=False):

        print "proceed_to_next_image", skip_junk

        def got_error(error, url):
            print "proceed_to_next_image got_error", error, url
            return error

        def wait(error):
            print "proceed_to_next_image got_error, wait 60s and re-start_loop"
            reactor.callLater(60, self.loop_start, now=False)


        if len(self.images) > 0:
            self.loop_stop()
            while True:
                try:
                    self.image_index += 1
                    image = self.images[self.image_index]
                except IndexError:
                    self.image_index = 0
                    image = self.images[self.image_index]
                print image
                if skip_junk == True and image.has_key('junk'):
                    continue
                image_url = image.get('url')
                image_title = image.get('title')
                d = client.getPage(image_url)
                d.addCallback(self.got_image, image_title)
                d.addErrback(got_error, image_url)
                d.addCallback(self.loop_start)
                d.addErrback(wait)
                break

    def proceed_to_previous_image(self, skip_junk=False):

        def got_error(error, url):
            print "proceed_to_previous_image got_error", error, url
            self.loop_start(None, now=False)
            return error

        if len(self.images) > 0:
            self.loop_stop()
            while True:
                try:
                    self.image_index -= 1
                    image = self.images[self.image_index]
                except IndexError:
                    self.image_index = len(self.images) - 1
                    image = self.images[self.image_index]
                print image
                if skip_junk == True and image.has_key('junk'):
                    continue
                image_url = image.get('url')
                image_title = image.get('title')
                d = client.getPage(image_url)
                d.addCallback(self.got_image, image_title)
                d.addCallback(self.loop_start)
                d.addErrback(got_error, image_url)
                break

    def got_image(self, result, title=''):
        try:
            import Image
            import StringIO
            im = Image.open(StringIO.StringIO(result))
            size = im.size
            self.current_image = pygame.image.frombuffer(im.tostring("raw", "RGB"), im.size, "RGB")
        except ImportError:
            import tempfile
            tmp_fp, tmp_path = tempfile.mkstemp()
            os.write(tmp_fp, result)
            os.close(tmp_fp)
            self.current_image = pygame.image.load(tmp_path).convert()
            size = self.current_image.get_size()
            os.unlink(tmp_path)

        transform = None
        screen_width = self.screen.get_width() - self.controller.width_redux
        screen_height = self.screen.get_height() - self.controller.height_redux
        if size[0] > screen_width:
            new_width = screen_width
            relation = float(screen_width()) / float(size[0])
            new_heigth = float(size[1]) * relation
            new_height = int(new_height)
            if new_height > screen_height:
                relation = float(screen_height) / float(new_height)
                new_width = float(new_width) * relation
                new_height = self.screen.get_height()
            transform = (new_width, new_height)
        elif size[1] > screen_height:
            relation = float(screen_height) / float(size[1])
            new_height = screen_height
            new_width = int(float(size[0]) * relation)
            transform = (new_width, new_height)
        if transform is not None:
            self.current_image = pygame.transform.scale(self.current_image, transform)
        if self.displaying is not None:
            #print "clear old image", self.displaying
            self.controller.clear_part(self.displaying)
        self.displaying = self.current_image.get_rect(center=((screen_width / 2), (screen_height / 2)))
        self.controller.redraw_part(self.current_image, self.displaying, screen=self.screen)
        self.current_title = title
        pygame.display.set_caption('Compère - %s' % title)
