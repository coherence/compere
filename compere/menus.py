# -*- coding: utf-8 -*-

# Licensed under the MIT license
# http://opensource.org/licenses/mit-license.php

# Copyright 2007, Frank Scholz <coherence@beebits.net>

import os

# Twisted
from twisted.internet import reactor
from twisted.internet import task
from twisted.python.util import sibpath
from twisted.web import client

# Pygame
import pygame
from pygame.locals import *

from resource import resource


class Menu(object):

    def __init__(self, controller, location, buttons):
        self.image = pygame.image.load(resource(('data', 'menu-frame-%s.png' % location))).convert()
        #sibpath(__file__, os.path.join('data','menu-frame-%s.png' % location)))
        self.image.set_colorkey((0, 0, 0), pygame.RLEACCEL)
        self.rect = self.image.get_rect()
        self.x_offset = 0
        if location == 'right':
            self.x_offset = 705
        self.rect[0] = self.x_offset
        self.y_offset = 0
        if location == 'bottom':
            self.y_offset = 410
        self.rect[1] = self.y_offset

        self.select_frame = pygame.image.load(resource(('data', 'button-selected.png'))).convert()
        self.select_frame.set_colorkey((255, 255, 255), pygame.RLEACCEL)

        self.unselect_frame = pygame.image.load(resource(('data', 'button-unselected.png'))).convert()
        self.unselect_frame.set_colorkey((255, 255, 255), pygame.RLEACCEL)

        self.pressed_frame = pygame.image.load(resource(('data', 'button-pressed.png'))).convert()
        self.pressed_frame.set_colorkey((255, 255, 255), pygame.RLEACCEL)

        self.controller = controller
        self.visible = False
        self.hideme = None

        self.buttons = []
        for args in buttons:
            button = Button(self, *args)
            self.buttons.append(button)
            self.image.blit(button.image, button.rect)

    def redraw(self, pos=None):
        #print "menu redraw", pos
        if self.visible == True:
            self.controller.display_redraw_part(self.image, self.rect)
            selected = None
            for b in self.buttons:
                if b.selected == True:
                    selected = b
                    #print "selected", selected.rect
            if pos is not None:
                for b in self.buttons:
                    if b.rect.collidepoint(pos[0] - self.rect[0], pos[1]):
                        if b.action == None:
                            return
                        #print "we have a touchdown"
                        if selected is not b:
                            #print "selected is not b"
                            if selected is not None:
                                #print "remove old select frame"
                                selected.selected = False
                                selected.transit = False
                                self.controller.display_redraw_part(self.unselect_frame, (selected.rect[0] + self.x_offset - 3, selected.rect[1] + self.y_offset - 3, 54, 54))
                            #print "draw new selected frame"
                            self.controller.display_redraw_part(self.select_frame, (b.rect[0] + self.x_offset - 3, b.rect[1] + self.y_offset - 3, 54, 54))
                            b.selected = True
                        return
                if selected is not None:
                    #print "clear old select frame"
                    self.controller.display_redraw_part(self.unselect_frame, (selected.rect[0] + self.x_offset - 3, selected.rect[1] + self.y_offset - 3, 54, 54))
                    selected.selected = False
                    selected.transit = False
            else:
                if selected is not None:
                    #print "redraw selected frame"
                    if hasattr(selected, 'transit') and selected.transit == True:
                        frame = self.pressed_frame
                    else:
                        frame = self.select_frame
                    self.controller.display_redraw_part(frame, (selected.rect[0] + self.x_offset - 3, selected.rect[1] + self.y_offset - 3, 54, 54))

    def show(self, pos):
        if self.hideme is not None and self.hideme.active == True:
            try:
                self.hideme.cancel()
            except:
                pass
            self.hideme = None
        if self.visible == False:
            self.visible = True
        self.redraw(pos)

    def hide(self):
        if self.visible == True:
            self.visible = False
            self.controller.display_clear_part(self.rect)
            for b in self.buttons:
                if b.selected == True:
                    b.selected = False
                    b.transit = False
                    self.image.blit(self.unselect_frame, (b.rect[0] - 3, b.rect[1] - 3, 54, 54))

    def prepare2hide(self):
        self.hideme = reactor.callLater(2, self.hide, )

    def check(self, button, pos):
        if self.visible == False:
            return None

        def clear_pressed(pressed):
            if pressed.selected == True:
                #print "clear_pressed"
                pressed.transit = False
                if self.visible == True:
                    self.controller.display_redraw_part(self.select_frame,
                                                        (pressed.rect[0] + self.x_offset - 3, pressed.rect[1] + self.y_offset - 3, 54, 54))

        for b in self.buttons:
            if(b.rect.collidepoint(pos[0] - self.rect[0], pos[1]) and
                b.action is not None):
                if b.transit == True:
                    return
                self.controller.display_redraw_part(self.pressed_frame, (b.rect[0] + self.x_offset - 3, b.rect[1] + self.y_offset - 3, 54, 54))
                b.transit = True
                reactor.callLater(1.4, clear_pressed, b)
                b.call()

    def toggle(self, image, rect):
        #self.controller.background.clear_part((rect[0]+self.x_offset,rect[1]+self.y_offset,rect[2],rect[3]))
        self.controller.clear_part((rect[0] + self.x_offset, rect[1] + self.y_offset, rect[2], rect[3]))
        self.image.blit(self.controller.background.background, rect, (rect[0] + self.x_offset, rect[1] + self.y_offset, rect[2], rect[3]))
        self.image.blit(image, rect)
        self.controller.display_redraw_part(self.image, (rect[0] + self.x_offset, rect[1] + self.y_offset, rect[2], rect[3]), rect)
        #pygame.display.update((rect[0]+self.x_offset,rect[1]+self.y_offset,rect[2],rect[3]))

    def set_button_action(self, name, action):
        for b in self.buttons:
            if b.name == name:
                b.action = action


class Button(object):

    def __init__(self, menu, name, position, action, alt_imagefile):
        self.menu = menu
        self.name = name
        self.image = pygame.image.load(resource(('data', '%s.png' % name))).convert()
        self.image.set_colorkey((0, 0, 0), pygame.RLEACCEL)
        self.rect = self.image.get_rect()
        self.rect[0] = position[0]
        self.rect[1] = position[1]
        self.action = action
        self.selected = False
        self.transit = False

        if alt_imagefile is not None:
            self.alt_image = pygame.image.load(resource(('data', alt_imagefile))).convert()
            self.alt_image.set_colorkey((0, 0, 0), pygame.RLEACCEL)
            self.toggle = self.alt_image

    def call(self):
        if self.action is not None:
            self.action()
        if hasattr(self, 'toggle'):
            self.menu.toggle(self.toggle, self.rect)
            if self.toggle == self.image:
                self.toggle = self.alt_image
            else:
                self.toggle = self.image
