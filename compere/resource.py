# -*- coding: utf-8 -*-

# Licensed under the MIT license
# http://opensource.org/licenses/mit-license.php

# Copyright 2007, Frank Scholz <coherence@beebits.net>

from pkg_resources import resource_stream
from compere import __pgk_name__


def resource(name):
    return resource_stream(__pgk_name__, '/'.join(name))
