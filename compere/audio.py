# -*- coding: utf-8 -*-

# Licensed under the MIT license
# http://opensource.org/licenses/mit-license.php

# Copyright 2007, Frank Scholz <coherence@beebits.net>

import os
import random
import time

# Twisted
from twisted.internet import reactor
from twisted.internet import task
from twisted.web import client

# Pygame
import pygame
from pygame.locals import *

# Coherence
from coherence.base import Coherence
from coherence.upnp.core import DIDLLite
import louie

from menus import Menu


class Cover(object):

    width = 260
    height = 260

    def __init__(self, uri):

        self.uri = uri
        self.finished = False
        self.image = pygame.Surface((self.width, self.height))
        self.image.fill((0, 0, 0))  # black
        self.rect = self.image.get_rect()

        def got_error(f):
            self.finished = True
            louie.send('MediaRenderer.Cover.finished', None, None)

        if self.uri is not None:
            d = client.getPage(self.uri)
            d.addCallback(self.got_cover)
            d.addErrback(got_error)

    def got_cover(self, result):
        try:
            import Image
            import StringIO
            im = Image.open(StringIO.StringIO(result))
            self.image = pygame.image.frombuffer(im.tostring("raw", "RGB"), im.size, "RGB")
        except ImportError:
            import tempfile
            tmp_fp, tmp_path = tempfile.mkstemp()
            os.write(tmp_fp, result)
            os.close(tmp_fp)
            self.image = pygame.image.load(tmp_path).convert()
            os.unlink(tmp_path)

        self.image = pygame.transform.scale(self.image, (self.width, self.height))
        self.rect = self.image.get_rect()
        self.finished = True
        louie.send('MediaRenderer.Cover.finished', None, None)


class TrackView(object):

    def __init__(self, client, id, title, cover, width, height):
        print "TrackView init", client, id
        self.client = client
        self.id = id
        self.title = title
        d = self.client.content_directory.browse(id, browse_flag='BrowseDirectChildren',
                                                     backward_compatibility=False)
        d.addCallback(self.process_album_browse, client)
        self.image = pygame.Surface((width, height))
        self.image.fill((0, 0, 0))  # black
        self.rect = self.image.get_rect()
        self.image.blit(cover.image, (100, 70))

    def process_album_browse(self, results, client):
        for k, v in results.iteritems():
            #print k, v
            if k == 'items':
                for id, values in v.iteritems():
                    #print values
                    if values['upnp_class'].startswith('object.item.audioItem'):
                        id = values['id']
                        title = values.get('title', 'untitled')
                        artist = values.get('artist', 'unknown')
                        cover = values.get('album_art_uri', None)
                        print "track", id, title, artist, cover
                louie.send('MediaRenderer.TrackView.ready', None, self)


class Album(object):

    width = 100
    height = 100

    def __init__(self, client, id, title, artist, cover_uri):
        self.id = id
        self.client = client
        self.title = title
        self.artist = artist
        self.cover = Cover(cover_uri)
        self.image = pygame.Surface((self.width, self.height))
        self.image.fill((0, 0, 0))  # black
        self.rect = self.image.get_rect()
        self.finished = False
        self.touched = 0
        print "album created", id, title, artist, cover_uri
        louie.connect(self.ready, 'MediaRenderer.Cover.finished', louie.Any)

    def ready(self, result):
        self.finished = True
        self.image = pygame.transform.scale(self.cover.image, (self.width, self.height))
        self.rect = self.image.get_rect()
        louie.send('MediaRenderer.Album.cover', None, None)


class Audio(object):

    album_x_offset = 100
    album_y_offset = 70

    def __init__(self, controller, main_rect):
        self.controller = controller
        self.screen = pygame.Surface((main_rect.width, main_rect.height))
        self.container_watch = []
        self.client = {}
        self.local_store = None
        self.content = []
        self.view = 0   # 0 = albums
        # 1 = track list
        self.track_view = None
        self.toggle_to = None

        buttons = (('save', (44, 50), None, None),
                   ('backward', (31, 140), self.proceed_to_previous_song, None),
                   ('pause', (31, 216), self.pause, None),
                   ('play', (31, 300), self.play, 'stop.png'),
                   ('forward', (44, 380), self.proceed_to_next_song, None))
        self.right_menu = Menu(controller, 'right', buttons)

        louie.connect(self.media_server_found, 'Coherence.UPnP.ControlPoint.MediaServer.detected', louie.Any)
        louie.connect(self.media_server_removed, 'Coherence.UPnP.ControlPoint.MediaServer.removed', louie.Any)

        louie.connect(self.album_view_ready, 'MediaRenderer.Album.cover', louie.Any)
        louie.connect(self.track_view_ready, 'MediaRenderer.TrackView.ready', louie.Any)

    def check(self, button, pos):
        if self.view == 0:
            for album in self.content[:18]:
                if album.rect.collidepoint(pos[0], pos[1]):
                    print "touched album", album.id, album.title
                    t = time.time()
                    if album.touched > 0 and t - album.touched < 0.5:
                        print "double click"
                        album.touched = 0
                        TrackView(album.client, album.id, album.title, album.cover,
                                  self.screen.get_width(), self.screen.get_height())
                    else:
                        album.touched = time.time()

    def toggle_fullscreen(self):
        return

    def clear_screen(self):
        self.screen.fill((0, 0, 0))

    def track_view_ready(self, track_view):
        self.track_view = track_view
        self.draw_track_view()

    def draw_track_view(self):
        #self.clear_screen()
        self.screen.blit(self.track_view.image, (0, 0))
        self.view = 1
        self.toggle_to = 0
        louie.send('MediaRenderer.Screen.redraw', None, self.screen)
        pygame.display.set_caption('Compère - %s' % self.track_view.title)

    def album_view_ready(self, r):
        for album in self.content[:18]:
            if album.finished == False:
                return
        self.draw_album_view()

    def draw_album_view(self):
        self.clear_screen()
        column = 0
        row = 0
        for album in self.content[:18]:
            album.rect[0] = self.album_x_offset + (column * album.rect[2])
            album.rect[1] = self.album_y_offset + (row * album.rect[3])

            self.screen.blit(album.image, album.rect)
            column += 1
            if column == 6:
                row += 1
                column = 0
        self.view = 0
        if self.track_view is not None:
            self.toggle_to = 1
        louie.send('MediaRenderer.Screen.redraw', None, self.screen)
        pygame.display.set_caption('Compère - Albums')

    def draw_screen(self):
        print "audio draw_screen", self.track_view, self.toggle_to
        if(self.track_view is not None and
           self.toggle_to is not None):
            self.view = self.toggle_to
        if self.view == 0:
            self.draw_album_view()
        else:
            self.draw_track_view()

    def media_server_found(self, client, usn):
        print "audio - media_server_found", client.device.get_friendly_name()
        if 'Coherence Test Content' == client.device.get_friendly_name():
            self.client[usn] = client
            #d = client.content_directory.browse(0, browse_flag='BrowseMetadata')
            d = self.client[usn].content_directory.browse(0, browse_flag='BrowseDirectChildren',
                                                        backward_compatibility=False)
            d.addCallback(self.process_media_server_browse, client)
            client.content_directory.subscribe_for_variable('ContainerUpdateIDs', self.state_variable_change)
            client.content_directory.subscribe_for_variable('SystemUpdateID', self.state_variable_change)

    def media_server_removed(self, usn):
        print "media_server_removed", usn
        if(isinstance(self.local_store, tuple) is True and
            len(self.local_store) == 2 and
            usn == self.local_store[0].device.get_usn()):
            self.local_store = None
            self.right_menu.set_button_action('save', None)
        if self.client.has_key(usn) == True:
            del self.client[usn]
            self.content = []

    def process_media_server_browse(self, results, client, albums=False):
        for k, v in results.iteritems():
            #print k, v
            if k == 'items':
                for id, values in v.iteritems():
                    print values
                    if(values['upnp_class'].startswith('object.container') and
                       values['title'] == 'audio'):
                        d = client.content_directory.browse(id, browse_flag='BrowseDirectChildren',
                                                                 backward_compatibility=False)
                        d.addCallback(self.process_media_server_browse, client, albums=True)
                    if albums == True and values['upnp_class'].startswith('object.container'):
                        id = values['id']
                        title = values.get('title', 'untitled')
                        artist = values.get('artist', 'unknown')
                        cover = values.get('album_art_uri', None)
                        self.content.append(Album(client, id, title, artist, cover))
                        if values['parent_id'] not in self.container_watch:
                            self.container_watch.append(values['parent_id'])

    def process_local_media_server_browse(self, results, client):
        for k, v in results.iteritems():
            #print k, v
            if k == 'items':
                for id, values in v.iteritems():
                    #print values
                    if(values['upnp_class'].startswith('object.container') and
                       values['title'] == 'content'):
                        d = client.content_directory.browse(id, browse_flag='BrowseDirectChildren',
                                                                 backward_compatibility=False)
                        d.addCallback(self.process_local_media_server_browse, client)
                    if(values['upnp_class'].startswith('object.container') and
                       values['title'] == 'audio'):
                        print "found object id", values['id'], "as storage point"
                        if hasattr(self, 'right_menu'):
                            self.right_menu.set_button_action('save', self.store_locally)
                        self.local_store = (self.local_store, values['id'])

    def state_variable_change(self, variable, usn):
        print variable.name, 'changed from', variable.old_value, 'to', variable.value
        if variable.old_value == '':
            return
        if variable.name == 'SystemUpdateID':
            if self.client.has_key(usn) == True:
                if len(self.content) == 0:
                    d = self.client[usn].content_directory.browse(0, browse_flag='BrowseDirectChildren',
                                                             backward_compatibility=False)
                    d.addCallback(self.process_media_server_browse, self.client[usn])

        if variable.name == 'ContainerUpdateIDs':
            changes = variable.value.split(',')
            while len(changes) > 1:
                container = changes.pop(0).strip()
                update_id = changes.pop(0).strip()
                if container in self.container_watch:
                    print "heureka, we have a change in ", container, ", container needs a reload"
                    self.content = []
                    d = self.client[usn].content_directory.browse(container, browse_flag='BrowseDirectChildren',
                                                             backward_compatibility=False)
                    d.addCallback(self.process_media_server_browse, self.client[usn], albums=True)

    def pause(self):
        """ pause audio play """

    def play(self):
        """ start or stop audio play """

    def proceed_to_next_song(self, skip_junk=False):
        """ skip one song forward """

    def proceed_to_previous_song(self, skip_junk=False):
        """ skip one song backward """
