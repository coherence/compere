# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

from compere import __version__

setup(
    name="Compere",
    version=__version__,
    description="MediaRenderer based on the Coherence DLNA/UPnP framework",
    long_description="""
""",
    author="Frank Scholz",
    author_email='coherence@beebits.net',
    license="MIT",
    install_requires=['Coherence', 'Twisted', 'setuptools', 'pygame',
                        'Louie', 'osso', 'Image | PIL'],
    packages=find_packages(),
    scripts=['bin/compere'],
    url="http://coherence.beebits.net",
    download_url='https://coherence.beebits.net/download/Compere-%s.tar.gz' % __version__,
    keywords=['UPnP', 'DLNA', 'multimedia', 'gstreamer'],
    classifiers=['Development Status :: 4 - Beta',
                   'Environment :: Console',
                   'Environment :: Web Environment',
                   'Environment :: Maemo',
                   'License :: OSI Approved :: MIT License',
                   'Operating System :: OS Independent',
                   'Programming Language :: Python',
                ],
    package_data={
        'compere': ['data/*.png'],
    }
)
